import React, { Component } from "react";
import "./index.css";
import todosList from "./todos.json";
import TodoList from "./TodoList.jsx";

class App extends Component {
  state = {
    todos: todosList,
    input: ''
  };
  //toggle todo complete
  handleToggleClick = todo => event => {
    let tempState = this.state
    todo.completed = !todo.completed
    this.setState({ tempState })
  }
  //
  handleDestroyClick = todo => event => {
    let tempState = this.state
    let newTodos = tempState.todos.filter(
      item => item.id !== todo.id
    )
    this.setState({ todos: newTodos })
  }

  //create functionality clear completeclick
  handleClearCompleteClick = event => {
    let tempState = this.state
    let tempTodos = this.state.todos.filter(
      todo => todo.completed === false
    )
    tempState.todos = tempTodos
    this.setState({ tempState })
  }
  //create todo
  handleCreateTodo = event => {
    if (event.keyCode === 13) {
      let tempState = this.state;

      tempState.todos.push(
        {
          userId: 1,
          id: Math.ceil(Math.random() * 100000),
          title: tempState.input,
          completed: false
        }
      )
      //state of todo
      this.setState({ todos: tempState.todos, input: '' });
      event.target.value = ''
    }
  }
  //handle the change!!
  handleChange = event => {
    this.setState({ input: event.target.value })
  }
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input
            className="new-todo"
            placeholder="What needs to be done?"
            autofocus
            //was confused when terminal was like "error, autofocus what?" well this is why!
            value={this.state.input}
            onChange={this.handleChange}
            onKeyDown={this.handleCreateTodo}
          />
        </header>
        <TodoList todos={this.state.todos}
        handleToggleClick={this.handleToggleClick}
        handleDestroyClick={this.handleDestroyClick}/>
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          {/* MAKIN SURE THE CLEAR COMPLETE WORKS */}
          <button className="clear-completed" onClick={this.handleClearCompleteClick}>Clear completed</button>
        </footer>
      </section>
    );
  }
}
//CONVERSION!!!!!!
//THIS WAS THE OLD TODOITEM AND OLD TODOLIST(BEFORE WE SEPERATED THEM INTO SEPERATE FILES)
// class TodoItem extends Component {
//   render() {
//     return (
//       <li className={this.props.completed ? "completed" : ""}>
//         <div className="view">
//           <input
//             className="toggle"
//             type="checkbox"
//             checked={this.props.completed}
//           />
//           <label>{this.props.title}</label>
//           <button className="destroy" />
//         </div>
//       </li>
//     );
//   }
// }

// class TodoList extends Component {
//   render() {
//     return (
//       <section className="main">
//         <ul className="todo-list">
//           {this.props.todos.map(todo => (
//             <TodoItem title={todo.title} completed={todo.completed} />
//           ))}
//         </ul>
//       </section>
//     );
//   }
// }

export default App;
